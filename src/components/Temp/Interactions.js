let data = [
    {
     id:"1",
     custName:"Mark",
    userName:"user1",
     subject:"Call Customer",
     creationDate:"2020-01-01",
     closeDate:"",
     status:"Open",
     comments:"called user, as he is busy reschedule it to 11/28/2010"
    },
    {
        id:"2",
        custName:"Mike",
        userName:"user1",
        subject:"Issue with claim",
        creationDate:"2020-11-11",
        closeDate:"",
        status:"Hold",
        comments:""
    },
    {
        id:"3",
        custName:"Mike",
        userName:"user1",
        subject:"Issue with claim",
        creationDate:"2020-11-11",
        closeDate:"",
        status:"Inprogress",
        comments:""
    },
    {
        id:"4",
        custName:"Mike",
        userName:"user1",
        subject:"Issue with claim",
        creationDate:"2020-11-11",
        closeDate:"",
        status:"Inprogress",
        comments:""
    },
    {
        id:"5",
        custName:"Mike",
        userName:"user1",
        subject:"Issue with claim",
        creationDate:"2020-11-11",
        closeDate:"",
        status:"Inprogress",
        comments:""
    },
    {
        id:"6",
        custName:"Mike",
        userName:"user1",
        subject:"Issue with claim",
        creationDate:"2020-11-11",
        closeDate:"",
        status:"Hold",
        comments:""
    },
    {
        id:"7",
        custName:"Mike",
        userName:"user1",
        subject:"Issue with claim",
        creationDate:"2020-11-11",
        closeDate:"2020-11-12",
        status:"Close",
        comments:""
    },
    {
        id:"8",
        custName:"Mike",
        userName:"user1",
        subject:"Issue with claim",
        creationDate:"2020-11-11",
        closeDate:"2020-12-11",
        status:"Close",
        comments:""
    }


]


export const getInteractionsAPI = () => {
      let promise = new Promise(function(resolve, reject) {
        resolve({data});
       //reject({});
      });

      return promise;
}


export const deleteInteractionsAPI = (id) => {
    data = data.filter(item => item["id"] !== id)
    let promise = new Promise(function(resolve, reject) {
        resolve({});
        //reject({})
      });

      return promise;
}


export const updateInteractionsAPI = (entry) => {
    data = data.filter(item => item["id"] !== entry["id"])
    data.push(entry)
    let promise = new Promise(function(resolve, reject) {
        resolve({});
       //reject({})
      });

      return promise;
}

export const addInteractionsAPI = (entry) => {
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (dt + Math.random()*16)%16 | 0;
        dt = Math.floor(dt/16);
        return (c=='x' ? r :(r&0x3|0x8)).toString(16);
    });

    data.push({id:uuid,...entry})
    let promise = new Promise(function(resolve, reject) {
        resolve({});
       //reject({})
      });

      return promise;
}