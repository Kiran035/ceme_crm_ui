import React from 'react'
import {  updateInteractions, addInteractions} from "../../store/InteractionActions";
import { connect } from 'react-redux'
class InteractionForm extends React.Component {

    constructor(props) {
        super(props)
        const date  = new Date()
        this.currDate = `${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()}`
        this.state ={
            custName:"",
            userName:"",
            subject:"",
            creationDate:this.currDate,
            status:"Open",
            closeDate:"",
            comments:"",
            ...props.data
        }

        this.isUpdateRequest = this.props.data["status"] ? true:false

   }

  
    handleChange = (event) => {
        const key = event.target.id  
        const value = event.target.value   
        this.setState({[key]:value})
    }

    handleStatusChange = (event) => {
        const key = event.target.id  
        const value = event.target.value 
        const obj = value==="Close" ?  {[key]:value, closeDate:this.currDate} :  {[key]:value, closeDate:""}
        this.setState(obj)
    }
   
    handleSubmit = (event) => {
        event.preventDefault();
        const { dispatch } = this.props;  
        
        dispatch(this.isUpdateRequest?updateInteractions(this.state):addInteractions(this.state)).then(
            (res) => this.props.callback(),
            () => {}
            )
               
    }

    

    render() {
        return (<>
            <div className="py-5 text-center">
                <h2>Interaction Details</h2>
            </div>
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-6 bg-light ">
                        <form onSubmit={(e)=> this.handleSubmit(e)}>
                            <div className="form-group">
                                <label forhtml="custName">Customer Name</label>
                                <input required   type="text" onChange={(e) => this.handleChange(e)} value={this.state.custName} className="form-control" id="custName"  />
                            </div>

                            <div className="form-group">
                                <label forhtml="subject">Issue Description</label>
                                <input required  minLength="10" maxLength="50" onChange={(e) => this.handleChange(e)} value={this.state.subject} type="text" className="form-control" id="subject"  />
                            </div>

                            <div className="form-group">
                                <label forhtml="creationDate">Creation Date</label>
                                <input required   onChange={(e) => this.handleChange(e)} value={this.state.creationDate} type="date" className="form-control" id="creationDate"  />
                            </div>

                            <div className="form-group">
                                <label forhtml="userName">User Name</label>
                                <input required   onChange={(e) => this.handleChange(e)} value={this.state.userName} type="text" className="form-control" id="userName"/>
                            </div>
                            <div className="row" >
                            <div className="form-group col-md-4">
                                <label forhtml="status">Status</label>
                                <select required   onChange={(e) => this.handleStatusChange(e)} value={this.state.status} id="status" className="form-control">
                                    <option>Open</option>
                                    <option>Inprogress</option>
                                    <option>Hold</option>
                                    <option>Close</option>
                                </select>
                            </div>

                           { this.state.status === "Close" && <div className="form-group">
                                <label forhtml="closeDate">Close Date</label>
                                <input required disabled onChange={(e) => this.handleChange(e)} value={this.state.closeDate || this.currDate} type="date" className="form-control" id="closeDate"  />
                            </div>}
                            </div>

                            <div className="form-group">
                                <label forhtml="comments">Additional Details</label>
                                <textarea minLength="15" maxLength="100" onChange={(e) => this.handleChange(e)} value={this.state.comments} className="form-control" id="comments"  />
                            </div>

                            <button type="submit" className="btn btn-primary btn-lg">{this.isUpdateRequest?"Update":"Add"}</button>
                            <button type="reset" onClick={()=> this.props.callback()} className="btn btn-secondary btn-lg">Cancel</button>

                            {this.props.message && (<div className="alert alert-danger alert-dismissible fade show">
                                    <strong>Error!</strong> {this.props.message}
                                </div>)}
                        </form>
                    </div>
                </div>
            </div>
        </>
        )
    }
}

const mapStateToProps = (state) => {
    const { message } = state.interaction;
    return {
       
        message
    };
}

export default connect(mapStateToProps)(InteractionForm);
