
export const statusData = (data) => {

    let init = {
        open:0,
        close:0,
        inprogress:0,
        hold:0
    }
    data.reduce(function (r, a) {
        r[a.status] = r[a.status] || [];
        r[a.status].push(a);
        if(a.status.toLowerCase())
        {
            let c = init[a.status.toLowerCase()] || 0;
            init[a.status.toLowerCase()] = parseInt(c)+1
        }
       
        return r
    }, Object.create(null));

    return  [
        ['Status', 'Last 30 days'],
        ['Open', init["open"]],
        ['Hold', init["hold"]],
        ['Inprogress', init["inprogress"]],
        ['Close', init["close"]],
    ]
}