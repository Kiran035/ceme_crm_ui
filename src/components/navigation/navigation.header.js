import React from 'react'
import { useSelector, useDispatch } from "react-redux";
import { logOut } from '../store/LoginActions'
import { Link } from 'react-router-dom'

const HeaderNav = () => {
    const dispatch = useDispatch()
    const auth = useSelector((state) => state.auth)
    return (
        <nav className="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
            <Link className="navbar-brand col-md-3 col-lg-2 mr-0 px-3" to="/">Allstate</Link>

            <ul className="navbar-nav px-3">
                <li className="nav-item text-nowrap">
                    {auth.user === null ? <a className="nav-link" href="/login">Login</a> :
                        <a className="nav-link" href="/login" onClick={() => dispatch(logOut())}>
                            <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-person-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path d="M13.468 12.37C12.758 11.226 11.195 10 8 10s-4.757 1.225-5.468 2.37A6.987 6.987 0 0 0 8 15a6.987 6.987 0 0 0 5.468-2.63z" />
                                <path fillRule="evenodd" d="M8 9a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" />
                                <path fillRule="evenodd" d="M8 1a7 7 0 1 0 0 14A7 7 0 0 0 8 1zM0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8z" />
                            </svg>
                            Logout
                        </a>}
                </li>
            </ul>
        </nav>
    )
}

export default HeaderNav


